import { join } from 'path';
import { Module } from '@nestjs/common';
import { MulterModule } from '@nestjs/platform-express';
import { ServeStaticModule } from '@nestjs/serve-static';

import { AppController } from './app.controller';
import { TarController } from './tar/tar.controller';
import { ZipController } from './zip/zip.controller';

@Module({
  imports: [
    MulterModule.register({
      dest: './files',
    }),
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, '..', 'forms', 'zip'),
      serveRoot: '/form/zip',
    }),
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, '..', 'forms', 'tar'),
      serveRoot: '/form/tar',
    }),
  ],
  controllers: [
    AppController,
    TarController,
    ZipController,
  ],
})
export class AppModule {}