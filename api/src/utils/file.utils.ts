import { extname } from 'path';
import { unlink } from 'fs';

import { File, ServiceResponse } from '../proto/build/common';

export const editFileName = (req, file, callback) => {
  const name = file.originalname.split('.')[0];
  const fileExtName = extname(file.originalname);
  const randomName = Array(4)
    .fill(null)
    .map(() => Math.round(Math.random() * 16).toString(16))
    .join('');
  callback(null, `${name}-${randomName}${fileExtName}`);
};

export const deleteFiles = (response: ServiceResponse) => {
    let deletions = response.files.files.map((file: File) => {
        return deleteFilePromise(file.path);
    });

    // Add archive path
    deletions.push(deleteFilePromise(response.path));

    return Promise.all(deletions);
};

export const deleteFilePromise = (path: string) => {
    return new Promise((resolve, reject) => {
        try {
            unlink(path, err => {
                if (err) throw err;
                resolve(path);
            });
        } catch (err) {
            reject(err);
        }
    });
};