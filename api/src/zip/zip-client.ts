import { join } from 'path';
import { ClientOptions, Transport } from '@nestjs/microservices';

export const ZipGrpcClientOptions: ClientOptions = {
  transport: Transport.GRPC,
  options: {
    url: 'zip:50051',
    package: 'zip',
    protoPath: join(__dirname, '..', 'proto', 'zip.proto'),
    loader: {
      enums: String,
      objects: true,
      arrays: true,
    },
  },
};