import { Client, ClientGrpc } from '@nestjs/microservices';
import {
  Controller,
  Logger,
  OnModuleInit,
  Res,
  Post,
  UploadedFiles,
  UseInterceptors,
} from '@nestjs/common';
import { diskStorage } from 'multer';
import { FilesInterceptor } from '@nestjs/platform-express';
import { Response } from 'express';

import { deleteFiles, editFileName } from '../utils/file.utils';
import { ServiceResponse } from '../proto/build/common';
import { ZipService } from '../proto/build/zip';
import { ZipGrpcClientOptions } from './zip-client';

@Controller('zip')
export class ZipController implements OnModuleInit {
  private readonly logger = new Logger(ZipController.name);

  @Client(ZipGrpcClientOptions)
  private readonly client: ClientGrpc;

  private zipService: ZipService;

  onModuleInit() {
    this.zipService = this.client.getService<ZipService>('ZipService');
  }

  @Post()
  @UseInterceptors(
    FilesInterceptor('files', +process.env.MAX_FILE_LIMIT, {
      limits: {
        fileSize: +process.env.MAX_FILE_SIZE,
      },
      storage: diskStorage({
        destination: '/files',
        fileName: editFileName,
      }),
    }),
  )
  async uploadMultipleFiles (@UploadedFiles() files, @Res() res: Response) {
    this.logger.log(`Compressing ${JSON.stringify(files)}`);

    const zipResponse: ServiceResponse = await this.zipService.zip({ files }).toPromise();

    return res.download(zipResponse.path, 'download.zip', error => {
      if (error) {
        this.logger.error(error);
      }

      deleteFiles(zipResponse).catch(error => {
        this.logger.error(error);
      });
    });
  }
}