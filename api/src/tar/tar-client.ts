import { join } from 'path';
import { ClientOptions, Transport } from '@nestjs/microservices';

export const TarGrpcClientOptions: ClientOptions = {
  transport: Transport.GRPC,
  options: {
    url: 'tar:50051',
    package: 'tar',
    protoPath: join(__dirname, '..', 'proto', 'tar.proto'),
    loader: {
      enums: String,
      objects: true,
      arrays: true,
    },
  },
};