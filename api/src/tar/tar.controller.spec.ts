import { Test, TestingModule } from '@nestjs/testing';
import { TarController } from './tar.controller';

describe('TarController', () => {
  let controller: TarController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [TarController],
    }).compile();

    controller = module.get<TarController>(TarController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
