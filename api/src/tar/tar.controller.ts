import { Client, ClientGrpc } from '@nestjs/microservices';
import {
  Controller,
  Logger,
  OnModuleInit,
  Res,
  Post,
  UploadedFiles,
  UseInterceptors,
} from '@nestjs/common';
import { diskStorage } from 'multer';
import { FilesInterceptor } from '@nestjs/platform-express';
import { Response } from 'express';

import { deleteFiles, editFileName } from '../utils/file.utils';
import { ServiceResponse } from '../proto/build/common';
import { TarService } from '../proto/build/tar';
import { TarGrpcClientOptions } from './tar-client';

// TODO convert to two spaces

@Controller('tar')
export class TarController implements OnModuleInit {
  private readonly logger = new Logger(TarController.name);

    @Client(TarGrpcClientOptions)
    private readonly client: ClientGrpc;

    private tarService: TarService;

    onModuleInit() {
      this.tarService = this.client.getService<TarService>('TarService');
    }

    @Post()
    @UseInterceptors(
      FilesInterceptor('files', +process.env.MAX_FILE_LIMIT, {
        limits: {
          fileSize: +process.env.MAX_FILE_SIZE,
        },
        storage: diskStorage({
          destination: '/files',
          fileName: editFileName,
        }),
      }),
    )
    async uploadMultipleFiles (@UploadedFiles() files, @Res() res: Response) {
      this.logger.log(`Compressing ${JSON.stringify(files)}`);

      const tarResponse: ServiceResponse = await this.tarService.tar({ files }).toPromise();

      return res.download(tarResponse.path, 'download.tar.gz', error => {
        if (error) {
          this.logger.error(error);
        }

        deleteFiles(tarResponse).catch(error => {
          this.logger.error(error);
        });
      });
    }
}