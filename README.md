![zip proxy diagram](zip-proxy.png)

**zip-proxy** is a simple REST service used to zip/tar files.

It's really here so I can play with [NestJS](https://docs.nestjs.com/), [@nestjs/microservices](https://www.npmjs.com/package/@nestjs/microservices), and [@grpc/proto-loader](https://docs.nestjs.com/microservices/grpc#grpc).

## TODOs

* Better error handling
* Abstract proto files
* Stronger typing
* Better logging
* Directories?
* Fix spacing
* Basic tests
* Unzipping
