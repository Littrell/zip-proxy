import { ConfigModule } from '@nestjs/config';
import { Module } from '@nestjs/common';

import { TarModule } from './tar/tar.module';

@Module({
  imports: [
    ConfigModule.forRoot(),
    TarModule,
  ],
})
export class AppModule {}
