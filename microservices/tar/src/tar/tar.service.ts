import { createWriteStream } from 'fs';
import { Observable } from 'rxjs';
import { Injectable, Logger } from '@nestjs/common';
import { v4 as uuid } from 'uuid';

const archiver = require('archiver');

import { TarService } from '../proto/build/tar';
import { Files, ServiceResponse } from '../proto/build/common';


@Injectable()
export class TarServiceImplementation implements TarService {
    private readonly logger = new Logger(TarServiceImplementation.name);

    tar(files: Files): Observable<ServiceResponse> {
        return new Observable<ServiceResponse>(subscriber => {
            const filename = `${uuid()}.tar.gz`;

            this.logger.log(`Creating ${filename}...`);

            const path = `/files/${filename}`;
            const output = createWriteStream(path);
            const archive = archiver('tar', {
                gzip: true,
            });

            archive.on('warning', warning => {
                this.logger.warn(warning);
                throw new warning;
            });

            archive.on('error', error => {
                this.logger.error(error);;
                throw new error;
            });

            output.on('close', () => {
                this.logger.log(`${filename} created successfully.`);
                subscriber.next({
                    filename,
                    files,
                    path,
                });
                subscriber.complete();
            });

            archive.pipe(output);

            files.files.forEach(file => {
                archive.file(file.path, { name: file.originalname });
            });

            archive.finalize();
        });
    }
}
