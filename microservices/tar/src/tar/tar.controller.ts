import { Controller, Inject } from '@nestjs/common';
import { GrpcMethod } from '@nestjs/microservices';
import { Observable } from 'rxjs';

import { Files, ServiceResponse } from '../proto/build/common';
import { TarService } from '../proto/build/tar';

@Controller()
export class TarController {

  constructor(@Inject('TarService') private readonly tarService: TarService) {}

  @GrpcMethod('TarService', 'tar')
  tar(files: Files): Observable<ServiceResponse> {
    return this.tarService.tar(files);
  }
}