import { Module } from '@nestjs/common';

import { TarController } from './tar.controller';
import { TarServiceImplementation } from './tar.service';

@Module({
    controllers: [TarController],
    providers: [{ provide: 'TarService', useClass: TarServiceImplementation }],
})
export class TarModule {}
