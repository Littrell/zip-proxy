/* eslint-disable */
import { Observable } from 'rxjs';
import { ServiceResponse, Files } from './common';

export const protobufPackage = 'zip';

export interface ZipService {
  zip(request: Files): Observable<ServiceResponse>;
}
