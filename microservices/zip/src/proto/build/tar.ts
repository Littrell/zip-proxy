/* eslint-disable */
import { Observable } from 'rxjs';
import { ServiceResponse, Files } from './common';

export const protobufPackage = 'tar';

export interface TarService {
  tar(request: Files): Observable<ServiceResponse>;
}
