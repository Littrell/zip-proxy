/* eslint-disable */
export const protobufPackage = 'common';

export interface File {
  fieldname: string;
  originalname: string;
  encoding: string;
  mimetype: string;
  destination: string;
  filename: string;
  path: string;
  size: number;
}

export interface Files {
  files: File[];
}

export interface ServiceResponse {
  filename: string;
  files: Files | undefined;
  path: string;
}
