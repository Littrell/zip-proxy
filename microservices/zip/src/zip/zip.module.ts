import { Module } from '@nestjs/common';

import { ZipController } from './zip.controller';
import { ZipServiceImplementation } from './zip.service';

@Module({
    controllers: [ZipController],
    providers: [{ provide: 'ZipService', useClass: ZipServiceImplementation }],
})
export class ZipModule {}