import { createWriteStream } from 'fs';
import { Observable } from 'rxjs';
import { Injectable, Logger } from '@nestjs/common';
import { v4 as uuid } from 'uuid';

const archiver = require('archiver');

import { Files, ServiceResponse } from '../proto/build/common';
import { ZipService } from '../proto/build/zip';


@Injectable()
export class ZipServiceImplementation implements ZipService {
    private readonly logger = new Logger(ZipServiceImplementation.name);

    zip(files: Files): Observable<ServiceResponse> {
        return new Observable<ServiceResponse>(subscriber => {
            const filename = `${uuid()}.zip`;

            this.logger.log(`Creating ${filename}...`);

            const path = `/files/${filename}`;
            const output = createWriteStream(path);
            const archive = archiver('zip', {
                zlib: { level: 9 },
            });

            archive.on('warning', warning => {
                this.logger.warn(warning);
                throw new warning;
            });

            archive.on('error', error => {
                this.logger.error(error);
                throw new error;
            });

            output.on('close', () => {
                this.logger.log(`${filename} created successfully.`);
                subscriber.next({
                    filename,
                    files,
                    path,
                });
                subscriber.complete();
            });

            archive.pipe(output);

            files.files.forEach(file => {
                archive.file(file.path, { name: file.originalname });
            });

            archive.finalize();
        });
    }
}
