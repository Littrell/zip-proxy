import { Controller, Inject } from '@nestjs/common';
import { GrpcMethod } from '@nestjs/microservices';
import { Observable } from 'rxjs';

import { Files, ServiceResponse } from '../proto/build/common';
import { ZipService } from '../proto/build/zip';

@Controller()
export class ZipController {

  constructor(@Inject('ZipService') private readonly zipService: ZipService) {}

  @GrpcMethod('ZipService', 'zip')
  zip(files: Files): Observable<ServiceResponse> {
    return this.zipService.zip(files);
  }
}
