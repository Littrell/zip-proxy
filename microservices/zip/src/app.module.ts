import { ConfigModule } from '@nestjs/config';
import { Module } from '@nestjs/common';

import { ZipModule } from './zip/zip.module';

@Module({
  imports: [
    ConfigModule.forRoot(),
    ZipModule,
  ],
})
export class AppModule {}
