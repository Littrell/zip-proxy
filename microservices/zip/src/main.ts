import { join } from 'path';
import { NestFactory } from '@nestjs/core';
import { MicroserviceOptions, Transport } from '@nestjs/microservices';

import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.createMicroservice<MicroserviceOptions>(AppModule, {
    transport: Transport.GRPC,
    options: {
      url: '0.0.0.0:50051',
      package: 'zip',
      protoPath: join(__dirname, './proto/zip.proto'),
      loader: {
        enums: String,
        objects: true,
        arrays: true,
      }
    },
  });
  app.listen(null);
}
bootstrap();
