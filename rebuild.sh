#!/bin/sh

docker-compose kill \
&& docker-compose rm -f \
&& docker volume rm $(docker volume ls -q) \
&& docker system prune -af --volumes \
&& docker-compose up -d \
&& docker-compose logs -f